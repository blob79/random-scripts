#!/bin/bash -e
[ $# -eq 0 ] && (echo $0 target_folder '[input_folder fix_seconds]*')
target=$1
shift 
function date_file {
	[ $# -eq 2 ] || ( echo base_folder and fix_secs ; exit 1 ) 
	#date --date=@$(( $(date --date="$D" +%s) + 60 * 5 + 30))

	find $1 -type f -not -name '*.AVI' | while read f
	do
		dr=$(exiv2 $f | sed -nr -e "s/Image timestamp\s*:\s*([0-9]*):([0-9]*):([0-9]*)(.*)/\1-\2-\3\4/p")
		d=$(date --date=@$(( $(date --date="$dr" +%s) + $2 )) +%F_%T )
		echo "$d $f"
	done 
}
while [[ $# > 1 ]]
do
  date_file $1 $2
  shift 2
done | sort | cut -d" " -f2 | while read f
do
	j=$((i++)) 
	cp -va $f $target/$(printf "%0.6d" $j)_$(basename $f)	
done
